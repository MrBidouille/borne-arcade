# Borne Arcade

Une borne arcade de type "bartop" deux joueurs sur RSPI + recalbox.

## Budget
(Prix approximatifs pour se faire une idée, possibilité de faire de la récupération pour certains éléments)

* Electronique (pi + cables + alim) : 60€
* Boutons + plaque controle USB : 100€
* Ecran 70€
* Bois et quincaillerie : 100€

## Matériel necessaire 

* Raspberry Pi (2 ou 3)
* Ecran max 22 pouces
* 17 boutons arcade : (6 + start/sélect par joueurs plus un bouton retour menu) éclairés ou pas.
* 2 joystick
* Une platine de connexion boutons interface USB 2 players (achetés sur SmallCab)
* Une carte SD

* CP18mm pour les cotés
* MDF 6mm pour les différentes plaques
* MDF 3mm pour le fond

* Visserie bois
* colle bois
* agrapheuse électrique

* Vinyle pour la déco

## Animation des ateliers

Premier atelier borne arcade :

Durée : 6h (14h-20h)
Participants : 15 personnes
Prix : gratuit

Organisation :

*** Scéance 1 ***

* Premiere explication en groupe :
	* Tour de table
	* Explication de l'objectif de l'atelier
	* Présentation du matériel et des machines qui vont etre utilisées
	* Tour du lab pour les nouveaux
* Partage en deux groupes :
	* Groupe 1 : découpe à la fraiseuse, laser et fabrication du chassis.
	* Groupe 2 : Application du vinyle sur la plaque du pad, montage des boutons, câblage, test sur le pi et l'écran

*** Scéance 2 ***

* Montage du pad et de l'écran dans le chassis
* En parallèle : découpe des stickers de déco sur le coté de la borne
* Test du pad et écran monté
* Montage de tous les éléments restants dans la borne (raspberry, son, blocs d'alims...)
* Test
* Mise en place de la déco sur la borne
* En parallèle : Réglages des manettes, pads, mise en place des jeux supplémentaires

* Débrief de fin
* Photo de groupe
* Si il reste du temps : mini compétition sur la borne (il faut bien la tester...)

